# See book Figure 3.2
# i for case insensitive
# * zero or more    + 1 or more      ? 0 or 1
# ^ start of the line   $ end of the line 
# [] set, one of the elements in the set 
# () group 
# \d dig  \s space 

puts "8:25 PM" =~  /(\d\d?):(\d\d)\s*([ap])m$/i ? "yes" : "not"  
puts "#{$1}  #{$2}  #{$3} " 

# omit  ()
puts "8:25 PM" =~  /\d\d?:\d\d\s*[ap]m$/i ? "yes" : "not"  
puts "#{$1}"  # won't work  

puts "540 pm" =~  /(\d\d?):(\d\d)\s*([ap])m$/i ? "yes" : "not" 






