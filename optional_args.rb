def foo1(arg1, more_arg={})
    p arg1 if more_arg[:fancy]      
end
p "foo1"
foo1(2, :fancy => true)
foo1 2, :fancy=>true       

# using * (splat) arguments
p "foo2"
def foo2(arg1, arg2, *other_arg)    
  p other_arg  
  p other_arg[0]
end

foo2(1,2,3,4)
foo2(1,2,[3,4])


p "mymethod"
def mymethod(required_arg, *args)
  p required_arg 
  p args

end
mymethod "foo","bar",:fancy => true 
mymethod "foo"                      