def foo(arg1, more_args={})   
	print arg1
	p more_args
end

foo(1, [2,3,4])
foo(1, {:my2 => 2,:my3 => 3, :my4 => 4})

#foo(1, {2,3, :my4 =>4})   #ERROR 
#foo (1,2,3,4)   # ERROR

foo(1, [2, :my])

foo 1, [2, :my]   
