p nil.to_s.empty?

p "Hello".empty?

p "I am here".split(/\W+/)          #  \W  nonword character 
	                              #  +   one or more
p "I am&here".split(/\W+/)
p "I am&here".split(/\s+/)        
	
p 'I am here'.split(/\s+/).select{|s| s =~ /^[aeiou]/i} 


mystring = 'I am here'.split(/\s+/).select{|s| s =~ /^[aeiou]/i}.
      map{|s| s.downcase}       

print   "mystring = #{mystring} " 

